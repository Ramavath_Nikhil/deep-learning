import cv2
import numpy as np

img1 = cv2.imread('images/bike.jpg')
img2 = cv2.imread('images/mainlogo.png')



rows, cols, channels = img2.shape
roi = img1[0:rows, 0:cols]

img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
ret, mask = cv2.threshold(img2gray, 220, 225, cv2.THRESH_BINARY_INV)

mask_inv = cv2.bitwise_not(mask)

img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)



img2_forground = cv2.bitwise_and(img2, img2, mask=mask)
cv2.imshow("img2_forground", img2_forground)
dst = cv2.add(img1_bg, img2_forground)
# cv2.imshow("image", dst)
img1[0:rows, 0:cols] = dst

cv2.imshow("image", img1)
# weighted = cv2.addWeighted(img1, 0.6, img2, 0.4, 0)
cv2.waitKey(0)
cv2.destroyAllWindows()
